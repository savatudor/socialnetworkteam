package com.company.Business;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

import static org.junit.jupiter.api.Assertions.*;

class ServiceFriendshipsTest {
    static ServiceFriendships serviceFriendships;
    static String sql;
    private static Connection connection;
    private static Statement statement;


    @BeforeAll
    static void setUp() throws SQLException {
        serviceFriendships = new ServiceFriendships("jdbc:postgresql://localhost:5432/social_network_test", "postgres", "12345678");
        connection = DriverManager.getConnection("jdbc:postgresql://localhost:5432/social_network_test", "postgres", "12345678");
        statement = connection.createStatement();
    }

    @BeforeEach
    void setUpEach() {
        sql = "DELETE FROM friendships;";
        try {
            statement.executeUpdate(sql);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @AfterAll
    static void cleanUp() {
        sql = "DELETE FROM friendships;";
        try {
            statement.executeUpdate(sql);
        } catch (Exception e) {
            e.printStackTrace();
        }
        sql = "DELETE FROM users;";
        try {
            statement.executeUpdate(sql);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Test
    void size() {
        assertEquals(serviceFriendships.size(), 0);
    }

    @Test
    void isEmpty() {
        assertTrue(serviceFriendships.isEmpty());
    }

    @Test
    void all() {
        var all = serviceFriendships.all();
        assertTrue(all.isEmpty());
    }
}
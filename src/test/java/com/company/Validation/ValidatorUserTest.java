package com.company.Validation;

import com.company.Domain.User;
import com.company.Exceptions.ValidatorException;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ValidatorUserTest {
    static User user1, user2;
    static ValidatorUser validator;


    @BeforeAll
    static void setUp() {
        user1 = new User("Good", "User");
        user1.setId(1);
        user2 = new User("", "");
        user2.setId(2);
        validator = new ValidatorUser();
    }

    @Test
    void valideaza() {
        try {
            validator.valideaza(user1);
        } catch (ValidatorException e) {
            fail();
        }
        try {
            validator.valideaza(user2);
            fail();
        } catch (ValidatorException e) {
            assertEquals(e.getMessage(), "Invalid first name!\nInvalid last name!\n");
        }
    }
}
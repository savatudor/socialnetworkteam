package com.company.Validation;

import com.company.Domain.Friendship;
import com.company.Domain.User;
import com.company.Exceptions.ValidatorException;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ValidatorFriendshipTest {
    static Friendship fr1, fr2;
    static ValidatorFriendship validator;

    @BeforeAll
    static void setUp() {
        fr1 = new Friendship(1, 2);
        fr1.setId(1);
        fr2 = new Friendship(3, 3);
        fr2.setId(2);
        validator = new ValidatorFriendship();
    }

    @Test
    void valideaza() {
        try {
            validator.valideaza(fr1);
        } catch (ValidatorException e) {
            fail();
        }
        try {
            validator.valideaza(fr2);
            fail();
        } catch (ValidatorException e) {
            assertEquals(e.getMessage(), "Users must be different!\n");
        }
    }
}
package com.company.Domain;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

class UserTest {
    static User user1, user2, user3;

    @BeforeAll
    static void setUp() {
        user1 = new User("Sava", "Tudor");
        user1.setId(1);
        user2 = new User("Name1", "Surname1");
        user2.setId(2);
        user3 = new User("Name2", "Surname2");
        user3.setId(1);
    }

    @Test
    void getFirstName() {
        assertEquals(user1.getFirstName(), "Sava");
    }

    @Test
    void setFirstName() {
        assertEquals(user3.getFirstName(), "Name2");
        user3.setFirstName("Modified");
        assertEquals(user3.getFirstName(), "Modified");
    }

    @Test
    void getLastName() {
        assertEquals(user1.getLastName(), "Tudor");
    }

    @Test
    void setLastName() {
        assertEquals(user3.getLastName(), "Surname2");
        user3.setLastName("Modified");
        assertEquals(user3.getLastName(), "Modified");
    }

    @Test
    void testEquals() {
        assertEquals(user1.hashCode(), user3.hashCode());
        assertEquals(user1, user3);
        assertNotEquals(user1, user2);
    }

    @Test
    void testToString() {
        assertEquals(user1.toString(), "1 | Sava Tudor");
    }
}
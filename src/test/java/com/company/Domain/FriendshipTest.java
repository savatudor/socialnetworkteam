package com.company.Domain;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class FriendshipTest {
    static Friendship fr1, fr2, fr3;

    @BeforeAll
    static void setUp() {
        fr1 = new Friendship(1, 2);
        fr1.setId(1);
        fr2 = new Friendship(2, 3);
        fr2.setId(2);
        fr3 = new Friendship(2, 1);
        fr3.setId(1);
    }

    @Test
    void getUserA() {
        assertEquals(fr1.getUserA(), 1);
        assertEquals(fr2.getUserA(), 2);
    }

    @Test
    void getUserB() {
        assertEquals(fr1.getUserB(), 2);
        assertEquals(fr2.getUserB(), 3);
    }

    @Test
    void testEquals() {
        assertEquals(fr1, fr3);
        assertNotEquals(fr1, fr2);
    }

    @Test
    void testToString() {
        assertEquals(fr1.toString(), "1 | 1 2");
    }

    @Test
    void isPart() {
        assertTrue(fr1.isPart(1));
        assertTrue(fr1.isPart(2));
        assertFalse(fr1.isPart(3));
    }
}
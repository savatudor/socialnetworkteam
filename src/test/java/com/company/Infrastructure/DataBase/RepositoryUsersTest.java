package com.company.Infrastructure.DataBase;

import com.company.Domain.User;
import com.company.Exceptions.RepositoryException;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

import static org.junit.jupiter.api.Assertions.*;

class RepositoryUsersTest {
    static RepositoryUsers repositoryUsers;
    static User user1, user2, user3;
    private static Connection connection;
    private static Statement statement;
    private static String sql;


    @BeforeAll
    static void setUp() throws SQLException {
        repositoryUsers = new RepositoryUsers("jdbc:postgresql://localhost:5432/social_network_test", "postgres", "12345678");
        user1 = new User("user1", "nr1");
        user1.setId(1);
        user2 = new User("user2", "nr2");
        user2.setId(2);
        user3 = new User("user3", "nr3");
        user3.setId(3);
        connection = DriverManager.getConnection("jdbc:postgresql://localhost:5432/social_network_test", "postgres", "12345678");
        statement = connection.createStatement();
    }

    @BeforeEach
    void setUpEach() {
        sql = "DELETE FROM users;";
        try {
            statement.executeUpdate(sql);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @AfterAll
    static void cleanUp(){
        sql = "DELETE FROM users;";
        try {
            statement.executeUpdate(sql);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    void size() throws RepositoryException {
        assertEquals(repositoryUsers.size(), 0);
        repositoryUsers.add(1, user1);
        assertEquals(repositoryUsers.size(), 1);
        repositoryUsers.add(2, user2);
        assertEquals(repositoryUsers.size(), 2);
    }

    @Test
    void isEmpty() throws RepositoryException {
        assertTrue(repositoryUsers.isEmpty());
        repositoryUsers.add(1, user1);
        assertFalse(repositoryUsers.isEmpty());
    }

    @Test
    void add() throws RepositoryException {
        assertEquals(repositoryUsers.size(), 0);
        repositoryUsers.add(1, user1);
        assertEquals(repositoryUsers.size(), 1);
        repositoryUsers.add(2, user2);
        assertEquals(repositoryUsers.size(), 2);
        try {
            repositoryUsers.add(1, user1);
            fail();
        } catch (Exception e) {
            assertEquals(e.getMessage(), "Entity already exists!\n");
        }
    }

    @Test
    void remove() throws RepositoryException {
        repositoryUsers.add(1, user1);
        repositoryUsers.add(2, user2);
        assertEquals(repositoryUsers.size(), 2);
        repositoryUsers.remove(1);
        assertEquals(repositoryUsers.size(), 1);
        try {
            repositoryUsers.remove(1);
            fail();
        } catch (Exception e) {
            assertEquals(e.getMessage(), "Entity does not exist!\n");
        }
    }

    @Test
    void all() throws RepositoryException {
        repositoryUsers.add(1, user1);
        repositoryUsers.add(2, user2);
        var all = repositoryUsers.all();
        assertEquals(all.size(), 2);
        assertEquals(all.get(0), user1);
        assertEquals(all.get(1), user2);
    }

    @Test
    void find() throws RepositoryException {
        repositoryUsers.add(1, user1);
        repositoryUsers.add(2, user2);
        try {
            var found = repositoryUsers.find(1);
            assertEquals(found, user1);
        } catch (Exception e) {
            fail();
        }
        try {
            var found = repositoryUsers.find(3);
            fail();
        } catch (Exception e) {
            assertEquals(e.getMessage(), "Entity does not exist!\n");
        }
    }

    @Test
    void update() throws RepositoryException {
        repositoryUsers.add(1, user1);
        repositoryUsers.add(2, user2);
        try {
            repositoryUsers.update(1, user3);
            var found = repositoryUsers.find(1);
            assertEquals(found.getFirstName(), "user3");
            assertEquals(found.getLastName(), "nr3");
        } catch (Exception e) {
            fail();
        }
        try {
            repositoryUsers.update(3, user1);
            fail();
        } catch (Exception e) {
            assertEquals(e.getMessage(), "Entity does not exist!\n");
        }
    }

    @Test
    void getElements() throws RepositoryException {
        repositoryUsers.add(1, user1);
        repositoryUsers.add(2, user2);
        var map = repositoryUsers.getElements();
        assertEquals(map.size(), 2);
        assertTrue(map.containsKey(1));
        assertTrue(map.containsKey(2));
    }
}
package com.company.Infrastructure.DataBase;

import com.company.Domain.Friendship;
import com.company.Domain.User;
import com.company.Exceptions.RepositoryException;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

import static org.junit.jupiter.api.Assertions.*;

class RepositoryFriendshipsTest {
    static RepositoryFriendships repositoryFriendships;
    static RepositoryUsers repositoryUsers;
    static User user1, user2, user3;
    static Friendship fr1, fr2, fr3;
    private static Connection connection;
    private static Statement statement;
    private static String sql;

    @BeforeAll
    static void setUp() throws SQLException, RepositoryException {
        repositoryUsers = new RepositoryUsers("jdbc:postgresql://localhost:5432/social_network_test", "postgres", "12345678");
        repositoryFriendships = new RepositoryFriendships("jdbc:postgresql://localhost:5432/social_network_test", "postgres", "12345678");
        fr1 = new Friendship(1, 2);
        fr1.setId(1);
        fr2 = new Friendship(2, 3);
        fr2.setId(2);
        fr3 = new Friendship(1, 3);
        fr3.setId(3);
        user1 = new User("user1", "nr1");
        user1.setId(1);
        user2 = new User("user2", "nr2");
        user2.setId(2);
        user3 = new User("user3", "nr3");
        user3.setId(3);
        repositoryUsers.add(1, user1);
        repositoryUsers.add(2, user2);
        repositoryUsers.add(3, user3);
        connection = DriverManager.getConnection("jdbc:postgresql://localhost:5432/social_network_test", "postgres", "12345678");
        statement = connection.createStatement();
    }

    @BeforeEach
    void setUpEach() {
        sql = "DELETE FROM friendships;";
        try {
            statement.executeUpdate(sql);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @AfterAll
    static void cleanUp(){
        sql = "DELETE FROM friendships;";
        try {
            statement.executeUpdate(sql);
        } catch (Exception e) {
            e.printStackTrace();
        }
        sql = "DELETE FROM users;";
        try {
            statement.executeUpdate(sql);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    void size() throws RepositoryException {
        assertEquals(repositoryFriendships.size(), 0);
        repositoryFriendships.add(1, fr1);
        assertEquals(repositoryFriendships.size(), 1);
    }

    @Test
    void isEmpty() throws RepositoryException {
        assertTrue(repositoryFriendships.isEmpty());
        repositoryFriendships.add(1, fr1);
        assertFalse(repositoryFriendships.isEmpty());

    }

    @Test
    void add() throws RepositoryException {
        assertEquals(repositoryFriendships.size(), 0);
        repositoryFriendships.add(1, fr1);
        assertEquals(repositoryFriendships.size(), 1);
        repositoryFriendships.add(2, fr2);
        assertEquals(repositoryFriendships.size(), 2);
        try {
            repositoryFriendships.add(1, fr1);
            fail();
        } catch (Exception e) {
            assertEquals(e.getMessage(), "Entity already exists!\n");
        }
    }

    @Test
    void remove() throws RepositoryException {
        assertEquals(repositoryFriendships.size(), 0);
        repositoryFriendships.add(1, fr1);
        assertEquals(repositoryFriendships.size(), 1);
        repositoryFriendships.add(2, fr2);
        assertEquals(repositoryFriendships.size(), 2);
        repositoryFriendships.remove(1);
        assertEquals(repositoryFriendships.size(), 1);
        try {
            repositoryFriendships.remove(1);
            fail();
        } catch (Exception e) {
            assertEquals(e.getMessage(), "Entity does not exist!\n");
        }
    }

    @Test
    void all() throws RepositoryException {
        assertEquals(repositoryFriendships.size(), 0);
        repositoryFriendships.add(1, fr1);
        assertEquals(repositoryFriendships.size(), 1);
        repositoryFriendships.add(2, fr2);
        assertEquals(repositoryFriendships.size(), 2);
        var all = repositoryFriendships.all();
        assertEquals(all.get(0), fr1);
        assertEquals(all.get(1), fr2);
    }

    @Test
    void find() throws RepositoryException {
        repositoryFriendships.add(1, fr1);
        repositoryFriendships.add(2, fr2);
        var found = repositoryFriendships.find(1);
        assertEquals(found, fr1);
        try {
            repositoryFriendships.find(3);
            fail();
        } catch (Exception e) {
            assertEquals(e.getMessage(), "Entity does not exist!\n");
        }
    }

    @Test
    void update() throws RepositoryException {
        repositoryFriendships.add(1, fr1);
        repositoryFriendships.add(2, fr2);
        repositoryFriendships.update(1, fr3);
        var updated = repositoryFriendships.find(1);
        assertEquals(updated, fr3);
        try {
            repositoryFriendships.update(4, fr2);
            fail();
        } catch (Exception e) {
            assertEquals(e.getMessage(), "Entity does not exist!\n");
        }
    }

    @Test
    void getElements() throws RepositoryException {
        repositoryFriendships.add(1, fr1);
        repositoryFriendships.add(2, fr2);
        var map = repositoryFriendships.getElements();
        assertEquals(map.size(), 2);
        assertTrue(map.containsKey(1));
        assertTrue(map.containsKey(2));
    }
}
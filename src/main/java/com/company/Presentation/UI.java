package com.company.Presentation;

import com.company.Controller.Controller;
import com.company.Exceptions.RepositoryException;
import com.company.Exceptions.ValidatorException;

import java.util.Scanner;

public class UI {
    private Controller controller;

    /**
     * Constructor for the UI class
     */
    public UI(Controller cont) {
        this.controller = cont;
    }

    /**
     * Prints all users
     */
    private void allUsers() {
        controller.allUsers().forEach((user) -> System.out.println(user.toString()));
    }

    private void allFriendships() {
        controller.allFriendships().forEach(friendship -> System.out.println(friendship.toString()));
    }

    private void menu() {
        System.out.println("1.Show all users");
        System.out.println("2.Show all friendships");
        System.out.println("0.Exit");
    }

    /**
     * Reads a command and calls the appropriate function
     *
     * @throws ValidatorException
     * @throws RepositoryException
     */
    public void run() throws ValidatorException, RepositoryException {
        Scanner input = new Scanner(System.in);
        int cmd;
        do {
            menu();
            cmd = Integer.parseInt(input.nextLine());
            try {
                switch (cmd) {
                    case 1 -> allUsers();
                    case 2 -> allFriendships();
                }
            } catch (Exception e) {
                System.out.println(e.getMessage());
            }
        } while (cmd != 0);
    }
}

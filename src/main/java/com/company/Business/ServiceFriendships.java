package com.company.Business;

import com.company.Domain.Friendship;
import com.company.Domain.User;
import com.company.Infrastructure.DataBase.RepositoryFriendships;
import com.company.Infrastructure.Repository;
import com.company.Validation.ValidatorFriendship;

import java.sql.SQLException;
import java.util.ArrayList;

public class ServiceFriendships {
    Repository<Integer, Friendship> repository;
    ValidatorFriendship validatorFriendship;

    public ServiceFriendships(String url, String user, String password) {
        try {
            repository = new RepositoryFriendships(url, user, password);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    /**
     * @return an integer representing the size of the repository
     */
    public int size() {
        return repository.size();
    }

    /**
     * @return true if the repository is empty, false otherwise
     */
    public boolean isEmpty() {
        return repository.isEmpty();
    }

    /**
     * @return an ArrayList representing all the users in the repository
     */
    public ArrayList<Friendship> all() {
        return repository.all();
    }
}

package com.company.Business;


import com.company.Domain.User;
import com.company.Infrastructure.DataBase.RepositoryUsers;
import com.company.Infrastructure.Repository;
import com.company.Validation.ValidatorUser;

import java.sql.SQLException;
import java.util.ArrayList;

public class ServiceUsers {
    Repository<Integer, User> repository;
    ValidatorUser validator;

    /**
     * Default constructor
     */
    public ServiceUsers(String url, String user, String password) {
        try {
            this.repository = new RepositoryUsers(url, user, password);

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        this.validator = new ValidatorUser();
    }

    /**
     * @return an integer representing the size of the repository
     */
    public int size() {
        return repository.size();
    }

    /**
     * @return true if the repository is empty, false otherwise
     */
    public boolean isEmpty() {
        return repository.isEmpty();
    }

    /**
     * @return an ArrayList representing all the users in the repository
     */
    public ArrayList<User> all() {
        return repository.all();
    }

}



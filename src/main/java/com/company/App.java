package com.company;

import com.company.Controller.Controller;
import com.company.Exceptions.RepositoryException;
import com.company.Exceptions.ValidatorException;
import com.company.Presentation.UI;

public class App {
    public static void main(String[] args) throws ValidatorException, RepositoryException {
        Controller controller = new Controller("jdbc:postgresql://localhost:5432/social_network", "postgres", "12345678");
        UI ui = new UI(controller);
        ui.run();
    }
}

package com.company.Validation;


import com.company.Exceptions.ValidatorException;

public interface Validator<E> {
    public void valideaza(E entity) throws ValidatorException;
}
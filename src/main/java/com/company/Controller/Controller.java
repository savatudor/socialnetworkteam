package com.company.Controller;

import com.company.Business.ServiceFriendships;
import com.company.Business.ServiceUsers;
import com.company.Domain.Friendship;
import com.company.Domain.User;
import com.utils.Graph;

import java.util.ArrayList;
import java.util.List;

public class Controller {
    private ServiceUsers serviceUsers;
    private ServiceFriendships serviceFriendships;
    private Graph<Integer> network;

    public Controller(String url, String user, String password){
        serviceUsers = new ServiceUsers(url, user, password);
        serviceFriendships = new ServiceFriendships(url,user,password);
        network = new Graph<>();
        serviceUsers.all().forEach(u -> {
            network.addVertex(u.getId());
        });
        serviceFriendships.all().forEach(fr -> {
            network.addEdge(fr.getUserA(), fr.getUserB());
        });
    }

    /**
     * @return an ArrayList containing all the users
     */
    public ArrayList<User> allUsers() {
        return serviceUsers.all();
    }


    /**
     * @return an ArrayList containing all the friendships
     */
    public ArrayList<Friendship> allFriendships() {
        return serviceFriendships.all();
    }


}

package com.company.Exceptions;

public class EntityException extends Exception {
    public EntityException(String message) {
        super(message);
    }
}